package com.schibsted.testapp.domain.networking;

import com.schibsted.testapp.model.RecipeResponse;
import com.schibsted.testapp.model.Recipes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RemoteService {
    @GET("search")
    Call<Recipes> searchRecipes(@Query("key") String key, @Query("q") String searchText, @Query("page") String page);

    @GET("get")
    Call<RecipeResponse> getRecipe(@Query("key") String key, @Query("rId") String recipeId);
}