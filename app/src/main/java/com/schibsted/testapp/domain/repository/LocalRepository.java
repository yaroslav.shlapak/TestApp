package com.schibsted.testapp.domain.repository;


import com.schibsted.testapp.model.Recipe;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface LocalRepository {
    Completable add(Recipe recipe);

    Completable addAll(List<Recipe> recipe);

    Completable update(Recipe recipe);

    Completable updateAll(List<Recipe> recipe);

    Completable delete(Recipe recipe);

    Single<Recipe> getRecipe(String id);

    Single<List<Recipe>> getRecipes();
}
