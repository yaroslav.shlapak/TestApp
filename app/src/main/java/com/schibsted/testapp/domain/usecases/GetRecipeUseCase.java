package com.schibsted.testapp.domain.usecases;

import com.schibsted.testapp.domain.repository.LocalRepository;
import com.schibsted.testapp.domain.repository.RemoteRepository;
import com.schibsted.testapp.model.Recipe;
import com.schibsted.testapp.utils.Constants;

import io.reactivex.Single;
import timber.log.Timber;

public class GetRecipeUseCase {
    private final LocalRepository localRepository;
    private final RemoteRepository remoteRepository;

    public GetRecipeUseCase(LocalRepository localRepository, RemoteRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }

    public static boolean isValid(final String recipeId) {
        return recipeId != null /*|| !recipeId.matches("[0-9]+")*/
                && recipeId.length() >= Constants.RECIPE_ID_LENGTH;
    }

    public Single<Recipe> getRecipe(String recipeId) {
        if (!isValid(recipeId)) {
            return Single.fromCallable(Recipe::getDefaultRecipe);
        }
        return remoteRepository
                .getRecipe(recipeId);
    }

    public Single<Recipe> getRecipeFromDb(String recipeId) {
        if (!isValid(recipeId)) {
            Timber.e("recipeId: %s", recipeId);
            return null;
        }
        return localRepository
                .getRecipe(recipeId);
    }
}
