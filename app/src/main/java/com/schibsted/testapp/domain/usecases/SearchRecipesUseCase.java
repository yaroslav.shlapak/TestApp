package com.schibsted.testapp.domain.usecases;

import com.schibsted.testapp.domain.repository.LocalRepository;
import com.schibsted.testapp.domain.repository.RemoteRepository;
import com.schibsted.testapp.model.Recipe;

import java.util.List;

import io.reactivex.Single;

public class SearchRecipesUseCase {
    private final LocalRepository localRepository;
    private final RemoteRepository remoteRepository;

    public SearchRecipesUseCase(LocalRepository localRepository, RemoteRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }

    public static boolean isValid(final String searchText) {
        return searchText != null && searchText.trim().length() > 0;
    }

    public Single<List<Recipe>> searchRecepies(String searchText, final int page) {

        if (!isValid(searchText)) {
            searchText = "";
        }

        return remoteRepository
                .searchRecipes(searchText, page)
                .flatMap((recipes -> {
                    localRepository.addAll(recipes).blockingAwait();
                    return Single.fromCallable(() -> recipes);
                }));

    }

}
