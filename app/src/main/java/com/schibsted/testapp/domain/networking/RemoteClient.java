package com.schibsted.testapp.domain.networking;

import android.support.annotation.NonNull;

import com.schibsted.testapp.model.Recipe;
import com.schibsted.testapp.model.RecipeResponse;
import com.schibsted.testapp.model.Recipes;
import com.schibsted.testapp.utils.Constants;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class RemoteClient {

    private static RemoteClient instance;

    private RemoteClient() {

    }

    public static synchronized RemoteClient getInstance() {
        if (instance == null) {
            instance = new RemoteClient();
        }
        return instance;
    }

    private Retrofit getRetrofitService(@NonNull String baseUrl) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }

    public Recipes searchRecipes(final int page, @NonNull final String searchText) throws IOException, RemoteException {
        RemoteService service = getRetrofitService(Constants.ENDPOINT).create(RemoteService.class);
        Timber.d("searchRecipes page: %s, searchText: ", page, searchText);

        // Remote call can be executed synchronously since the job calling it is already backgrounded.
        Response<Recipes> response = service.searchRecipes(Constants.API_KEY, searchText, "" + page).execute();

        if (response == null || !response.isSuccessful() || response.errorBody() != null) {
            if (response != null && response.errorBody() != null) {
                Timber.e("unsuccessful searchRecipes response: %s", response.errorBody().string());
            }
            throw new RemoteException(response);
        }

        Timber.d("successful searchRecipes response: %s", response.body().toString());

        return response.body();
    }

    public Recipe getRecipe(@NonNull String recipeId) throws IOException, RemoteException {
        RemoteService service = getRetrofitService(Constants.ENDPOINT).create(RemoteService.class);
        Timber.d("getRecipe with id: %s", recipeId);

        // Remote call can be executed synchronously since the job calling it is already backgrounded.
        Response<RecipeResponse> response = service.getRecipe(Constants.API_KEY, recipeId).execute();

        if (response == null || !response.isSuccessful() || response.errorBody() != null) {
            if (response != null && response.errorBody() != null) {
                Timber.e("unsuccessful getRecipe response: %s", response.errorBody().string());
            }
            throw new RemoteException(response);
        }

        Timber.d("successful getUser response: %s", response.body());

        return response.body().getRecipe();
    }

}

