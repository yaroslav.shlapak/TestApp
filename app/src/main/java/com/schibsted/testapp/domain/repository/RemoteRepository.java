package com.schibsted.testapp.domain.repository;

import com.schibsted.testapp.model.Recipe;

import java.util.List;

import io.reactivex.Single;

public interface RemoteRepository {
    Single<List<Recipe>> searchRecipes(String searchText, int page);

    Single<Recipe> getRecipe(String id);
}
