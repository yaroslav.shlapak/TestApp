package com.schibsted.testapp.di;

import com.schibsted.testapp.TestAppApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        /* Use AndroidInjectionModule.class if you're not using support library */
        AndroidSupportInjectionModule.class,
        AppModule.class,
        BuildersModule.class})
public interface AppComponent {
    void inject(TestAppApplication app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(TestAppApplication application);

        AppComponent build();
    }
}
