package com.schibsted.testapp.di;

import com.schibsted.testapp.view.RecipeActivity;
import com.schibsted.testapp.view.SearchRecipesActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Binds all sub-components within the app.
 */
@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector(modules = SearchRecepiesActivityModule.class)
    abstract SearchRecipesActivity bindSearchRecipesActivity();

    @ContributesAndroidInjector(modules = RecipeActivityModule.class)
    abstract RecipeActivity bindRecipeActivity();

    // Add bindings for other sub-components here
}
