package com.schibsted.testapp.di;


import com.schibsted.testapp.domain.repository.LocalRepository;
import com.schibsted.testapp.domain.repository.RemoteRepository;
import com.schibsted.testapp.domain.usecases.GetRecipeUseCase;
import com.schibsted.testapp.presentation.RecipeViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Define RecipeActivity-specific dependencies here.
 */
@Module
public class RecipeActivityModule {

    @Provides
    RecipeViewModelFactory provideCoursesViewModelFactory(GetRecipeUseCase getRecipeUseCase) {
        return new RecipeViewModelFactory(getRecipeUseCase);
    }

    @Provides
    GetRecipeUseCase provideGetCoursesUseCase(LocalRepository localRepository, RemoteRepository remoteRepository) {
        return new GetRecipeUseCase(localRepository, remoteRepository);
    }

}
