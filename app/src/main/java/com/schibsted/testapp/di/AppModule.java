package com.schibsted.testapp.di;

import android.content.Context;

import com.schibsted.testapp.TestAppApplication;
import com.schibsted.testapp.data.database.RecipeDao;
import com.schibsted.testapp.data.database.TestAppDatabase;
import com.schibsted.testapp.data.store.LocalDataStore;
import com.schibsted.testapp.data.store.RemoteDataStore;
import com.schibsted.testapp.domain.repository.LocalRepository;
import com.schibsted.testapp.domain.repository.RemoteRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This is where you will inject application-wide dependencies.
 */
@Module
public class AppModule {

    @Provides
    Context provideContext(TestAppApplication application) {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    RecipeDao provideUserDao(Context context) {
        return TestAppDatabase.getInstance(context).recipeDao();
    }

    @Singleton
    @Provides
    LocalRepository provideLocalRepository(RecipeDao commentDao) {
        return new LocalDataStore(commentDao);
    }

    @Singleton
    @Provides
    RemoteRepository provideRemoteRepository() {
        return new RemoteDataStore();
    }
}
