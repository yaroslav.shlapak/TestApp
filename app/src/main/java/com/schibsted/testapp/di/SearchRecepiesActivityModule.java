package com.schibsted.testapp.di;


import com.schibsted.testapp.domain.repository.LocalRepository;
import com.schibsted.testapp.domain.repository.RemoteRepository;
import com.schibsted.testapp.domain.usecases.SearchRecipesUseCase;
import com.schibsted.testapp.presentation.SearchRecipesViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Define SearchRecipesActivity-specific dependencies here.
 */
@Module
public class SearchRecepiesActivityModule {
    @Provides
    SearchRecipesViewModelFactory provideViewModelFactory(SearchRecipesUseCase searchRecipesUseCase) {
        return new SearchRecipesViewModelFactory(searchRecipesUseCase);
    }

    @Provides
    SearchRecipesUseCase provideSearchRecepiesUseCase(LocalRepository localRepository, RemoteRepository remoteRepository) {
        return new SearchRecipesUseCase(localRepository, remoteRepository);
    }

}
