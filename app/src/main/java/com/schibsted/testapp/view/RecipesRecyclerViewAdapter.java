package com.schibsted.testapp.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.schibsted.testapp.R;
import com.schibsted.testapp.model.Recipe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

class RecipesRecyclerViewAdapter extends RecyclerView.Adapter<RecipesRecyclerViewAdapter.ViewHolder> {

    private final List<Recipe> recipes;
    private Consumer<Recipe> onItemClickAction;

    public RecipesRecyclerViewAdapter(Consumer<Recipe> onItemClickAction) {
        this.recipes = new ArrayList<>();
        this.onItemClickAction = onItemClickAction;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.recipe_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Recipe recipe = recipes.get(position);
        holder.itemView.setOnClickListener(v -> {
                    Disposable d = Flowable
                            .just(recipe)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(onItemClickAction, throwable -> throwable.printStackTrace());
                }
        );
        holder.bind(recipe);
    }

    @Override
    public int getItemCount() {
        return recipes == null ? 0 : recipes.size();
    }

    public void putNewData(List<Recipe> recipes) {
        Timber.d("Got new recipes " + recipes.size());
        this.recipes.clear();
        this.recipes.addAll(recipes);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recipe_image)
        ImageView recipeCover;
        @BindView(R.id.recipe_name)
        TextView recipeName;

        View itemView;

        public ViewHolder(final View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }

        private void bind(final Recipe recipe) {

            recipeName.setText(recipe.getTitle());
            Picasso.with(itemView.getContext())
                    .load(recipe.getImageUrl())
                    .fit().centerCrop()
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(recipeCover);
        }
    }
}
