package com.schibsted.testapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.schibsted.testapp.R;
import com.schibsted.testapp.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.web_view)
    WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        String url = getUrlFromIntent();
        if (url != null) {
            webView.loadUrl(url);
        }
    }

    public @Nullable
    String getUrlFromIntent() {
        String recipeId = null;
        Intent intent = getIntent();
        if (intent.hasExtra(Constants.EXTRA_WEB_VIEW_URL)) {
            recipeId = intent.getStringExtra(Constants.EXTRA_WEB_VIEW_URL);
        }
        return recipeId;
    }

    @OnClick(R.id.toolbar_back)
    public void onViewClicked() {
        super.onBackPressed();
    }
}
