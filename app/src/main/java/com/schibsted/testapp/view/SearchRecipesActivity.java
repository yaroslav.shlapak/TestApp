package com.schibsted.testapp.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;

import com.schibsted.testapp.R;
import com.schibsted.testapp.model.Recipe;
import com.schibsted.testapp.presentation.SearchRecipesViewModel;
import com.schibsted.testapp.presentation.SearchRecipesViewModelFactory;
import com.schibsted.testapp.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import timber.log.Timber;

import static com.schibsted.testapp.utils.Constants.FIRST_PAGE;

public class SearchRecipesActivity extends AppCompatActivity {
    // UI references.
    @BindView(R.id.search_recipes_recycler_view)
    protected RecyclerView recyclerView;
    @BindView(R.id.search_recipes_search_text)
    protected AutoCompleteTextView serchText;
    @BindView(R.id.search_progress)
    protected ProgressBar progress;

    @Inject
    SearchRecipesViewModelFactory viewModelFactory;

    private SearchRecipesViewModel viewModel;
    private RecipesRecyclerViewAdapter recipesRecyclerViewAdapter;
    private EndlessScrollListener endlessScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_recipes);
        ButterKnife.bind(this);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchRecipesViewModel.class);

        serchText.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL || id == EditorInfo.IME_ACTION_SEARCH) {
                searchRecipes(serchText.getText().toString());
                hideSoftKeyboard();
                return true;
            }
            return false;
        });

        viewModel.getRecipesLiveData().observe(this, this::recipesReceived);

        initRecyclerView();
    }

    private void searchRecipes(final String searchText) {
        viewModel.searchRecipes(searchText, FIRST_PAGE);
        endlessScrollListener.resetState();
        showProgress();
    }

    private void recipesReceived(final List<Recipe> recipes) {
        hideProgress();
        Timber.d("got %d recipes", recipes.size());
        recipesRecyclerViewAdapter.putNewData(recipes);
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recipesRecyclerViewAdapter = new RecipesRecyclerViewAdapter((recipe) -> {
            openRecipeActivity(recipe.getRecipeId());
        });
        recyclerView.setAdapter(recipesRecyclerViewAdapter);
        endlessScrollListener = new EndlessScrollListener(layoutManager) {

            @Override
            public void onLoadMore(final int page, final int totalItemsCount, final RecyclerView view) {
                Timber.d("page: %d", page);
                viewModel.searchRecipes(serchText.getText().toString(), page + 1);
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
        endlessScrollListener.setPage(viewModel.getPage() - 1);
    }

    private void openRecipeActivity(final String recipeId) {
        Intent intent = new Intent(this, RecipeActivity.class);
        intent.putExtra(Constants.EXTRA_RECIPE_ID, recipeId);
        startActivity(intent);
    }

    private void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    private void showProgress() {
        progress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void hideProgress() {
        progress.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }
}
