package com.schibsted.testapp.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.schibsted.testapp.R;
import com.schibsted.testapp.model.Recipe;
import com.schibsted.testapp.presentation.RecipeViewModel;
import com.schibsted.testapp.presentation.RecipeViewModelFactory;
import com.schibsted.testapp.utils.Constants;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import timber.log.Timber;

public class RecipeActivity extends AppCompatActivity {

    // UI references.
    @BindView(R.id.recipe_preview_image)
    protected ImageView recipePreviewImage;
    @BindView(R.id.recipe_ingredients_recycler_view)
    protected RecyclerView recyclerView;
    @BindView(R.id.recipe_view_instruction_tv)
    protected TextView recipeViewInstructionTv;
    @BindView(R.id.recipe_view_original_tv)
    protected TextView recipeViewOriginalTv;

    @BindView(R.id.recipe_publisher_name)
    protected TextView recipePublisherName;
    @BindView(R.id.recipe_social_rank)
    protected TextView recipeSocialRank;
    @BindView(R.id.toolbar_title)
    protected TextView toolbarTitle;
    @BindView(R.id.recipe_scroll)
    protected ScrollView scroll;
    @BindView(R.id.recipe_progress)
    protected ProgressBar recipeProgress;
    @BindView(R.id.ingredients_progress)
    protected ProgressBar ingredientsProgress;
    @Inject
    RecipeViewModelFactory viewModelFactory;
    private RecipeViewModel viewModel;
    private IngredientsRecyclerViewAdapter recyclerViewAdapter;
    private String viewOriginalUrl;
    private String viewInstructionUrl;

    @OnClick(R.id.recipe_view_instruction_tv)
    protected void onViewInstructionClick() {
        if (viewInstructionUrl != null) {
            openWebViewActivity(viewInstructionUrl);
        }
    }

    @OnClick(R.id.toolbar_back)
    protected void onBackButtonPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.recipe_view_original_tv)
    protected void onViewOriginalClick() {
        if (viewOriginalUrl != null) {
            openWebViewActivity(viewOriginalUrl);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        ButterKnife.bind(this);

        String recipeId = getRecipeIdFromIntent();

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RecipeViewModel.class);
        viewModel.getRecipeLiveData().observe(this, this::recipeReceived);

        if (recipeId != null) {
            //viewModel.performGetRecipeFromDb(recipeId);
            viewModel.performGetRecipe(recipeId);
            showRecipeProgress();
        }

        viewOriginalUrl = null;
        viewInstructionUrl = null;
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));
        recyclerViewAdapter = new IngredientsRecyclerViewAdapter((recipe) -> {
            //
        });
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void recipeReceived(final Recipe recipe) {
        Timber.d("recipe with title: %s is receivred", recipe.getTitle());
        updateUI(recipe);
    }

    private void updateUI(final Recipe recipe) {
        hideRecipeProgress();

        Picasso.with(getApplicationContext())
                .load(recipe.getImageUrl())
                .fit().centerCrop()
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(recipePreviewImage);

        if (recipe.getIngredients() != null) {
            recyclerViewAdapter.putNewData(recipe.getIngredients());
            hideIngredientsProgress();
        } else {
            showIngredientsProgress();
        }
        recipePublisherName.setText(recipe.getPublisher());
        if (recipe.getSocialRank() != null) {
            recipeSocialRank.setText(String.format(getApplicationContext().
                    getString(R.string.recipe_social_rank), "" + recipe.getSocialRank().intValue()));
        }
        viewOriginalUrl = recipe.getSourceUrl();
        viewInstructionUrl = recipe.getF2fUrl();
        toolbarTitle.setText(recipe.getTitle());
    }

    public @Nullable
    String getRecipeIdFromIntent() {
        String recipeId = null;
        Intent intent = getIntent();
        if (intent.hasExtra(Constants.EXTRA_RECIPE_ID)) {
            recipeId = intent.getStringExtra(Constants.EXTRA_RECIPE_ID);
        }
        return recipeId;
    }

    private void openWebViewActivity(final String url) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(Constants.EXTRA_WEB_VIEW_URL, url);
        startActivity(intent);
    }

    private void showRecipeProgress() {
        recipeProgress.setVisibility(View.VISIBLE);
        scroll.setVisibility(View.GONE);
    }

    private void hideRecipeProgress() {
        recipeProgress.setVisibility(View.GONE);
        scroll.setVisibility(View.VISIBLE);
    }

    private void showIngredientsProgress() {
        if (scroll.getVisibility() == View.VISIBLE) {
            ingredientsProgress.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void hideIngredientsProgress() {
        if (scroll.getVisibility() == View.VISIBLE) {
            ingredientsProgress.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }
}
