package com.schibsted.testapp.data.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.schibsted.testapp.model.Recipe;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface RecipeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long add(Recipe recipe);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> addAll(List<Recipe> recipe);

    @Update
    void update(Recipe recipe);

    @Update
    void update(List<Recipe> recipe);

    @Delete
    void delete(Recipe recipe);

    @Query("SELECT * FROM recipe WHERE recipeId = :recipeId")
    Single<Recipe> getRecipe(String recipeId);

    @Query("SELECT * FROM recipe")
    Single<List<Recipe>> getRecipes();
}
