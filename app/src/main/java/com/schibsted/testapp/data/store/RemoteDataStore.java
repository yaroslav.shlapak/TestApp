package com.schibsted.testapp.data.store;

import android.support.annotation.NonNull;

import com.schibsted.testapp.domain.networking.RemoteClient;
import com.schibsted.testapp.domain.repository.RemoteRepository;
import com.schibsted.testapp.model.Recipe;
import com.schibsted.testapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;



public class RemoteDataStore implements RemoteRepository {

    @Override
    public Single<List<Recipe>> searchRecipes(@NonNull final String searchText, int page) {

        return Single.fromCallable(() -> {
            //TODO errors handling
            List<Recipe> recipes = new ArrayList<>();
            recipes.addAll(RemoteClient.getInstance().searchRecipes(page, searchText).getRecipes());
            return recipes;
        });

    }

    @Override
    public Single<Recipe> getRecipe(@NonNull final String recipeId) {
        return Single.fromCallable(() -> {
            //TODO errors handling
            return RemoteClient.getInstance().getRecipe(recipeId);
        });
    }
}
