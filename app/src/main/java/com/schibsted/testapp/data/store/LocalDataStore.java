package com.schibsted.testapp.data.store;

import android.support.annotation.NonNull;

import com.schibsted.testapp.data.database.RecipeDao;
import com.schibsted.testapp.domain.repository.LocalRepository;
import com.schibsted.testapp.model.Recipe;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import timber.log.Timber;

public class LocalDataStore implements LocalRepository {

    private final RecipeDao recipeDao;

    public LocalDataStore(RecipeDao recipeDao) {
        this.recipeDao = recipeDao;
    }

    @Override
    public Completable add(@NonNull final Recipe recipe) {
        Timber.d("adding recipe with id %s", recipe.getRecipeId());
        return Completable.fromAction(() -> {
            Timber.d("adding recipe");
            long l = recipeDao.add(recipe);
            Timber.d("added recipe with long %d", l);
        });
    }

    @Override
    public Completable update(@NonNull final Recipe recipe) {
        Timber.d("updating recipe with id %s", recipe.getRecipeId());
        return Completable.fromAction(() -> recipeDao.update(recipe));
    }

    @Override
    public Completable updateAll(final List<Recipe> recipes) {
        return Completable.fromAction(() -> recipeDao.update(recipes));
    }

    @Override
    public Completable addAll(final List<Recipe> recipes) {
        return Completable.fromAction(() -> recipeDao.addAll(recipes));
    }

    @Override
    public Completable delete(@NonNull final Recipe recipe) {
        Timber.d("deleting recipe with id %s", recipe.getRecipeId());
        return Completable.fromAction(() -> recipeDao.delete(recipe));
    }

    @Override
    public Single<List<Recipe>> getRecipes() {
        Timber.d("getting all recipes");
        return recipeDao.getRecipes();
    }

    @Override
    public Single<Recipe> getRecipe(String id) {
        Timber.d("getting recipe with id %s", id);
        return recipeDao.getRecipe(id);
    }


}