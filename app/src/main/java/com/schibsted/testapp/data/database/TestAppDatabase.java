package com.schibsted.testapp.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.schibsted.testapp.model.Recipe;

@Database(entities = {Recipe.class}, version = 1, exportSchema = false)
public abstract class TestAppDatabase extends RoomDatabase {
    private static TestAppDatabase instance;

    public static synchronized TestAppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room
                    .databaseBuilder(context.getApplicationContext(), TestAppDatabase.class, "testappdatabase")
                    .fallbackToDestructiveMigration() //replace by appropriate migration
                    .build();
        }
        return instance;
    }

    public abstract RecipeDao recipeDao();
}
