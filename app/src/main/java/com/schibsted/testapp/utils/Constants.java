package com.schibsted.testapp.utils;

final public class Constants {
    public static final String ENDPOINT = "http://food2fork.com/api/";
    public static final String EXTRA_RECIPE_ID = "EXTRA_RECIPE_ID";
    public static final String EXTRA_WEB_VIEW_URL = "EXTRA_WEB_VIEW_URL";
    public static final int RECIPE_ID_LENGTH = 2;
    private static final String API_KEY_MY = "5bc719e5cdfab96363068e35364515ff";
    private static final String API_KEY_FROM_TASK = "b549c4c96152e677eb90de4604ca61a2";
    public static final String API_KEY = API_KEY_FROM_TASK;

    public static final int FIRST_PAGE = 1;

    private Constants() {

    }
}
