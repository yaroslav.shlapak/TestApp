package com.schibsted.testapp.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity
public class Recipe {

    @NonNull
    @PrimaryKey
    @SerializedName("recipe_id")
    @Expose
    private String recipeId;

    @SerializedName("publisher")
    @Expose
    private String publisher;
    @SerializedName("f2f_url")
    @Expose
    private String f2fUrl;
    @SerializedName("source_url")
    @Expose
    private String sourceUrl;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("social_rank")
    @Expose
    private Double socialRank;
    @SerializedName("publisher_url")
    @Expose
    private String publisherUrl;
    @SerializedName("title")
    @Expose
    private String title;

    @Ignore
    @SerializedName("ingredients")
    @Expose
    private List<String> ingredients = null;

    public Recipe(final String publisher, final String f2fUrl, //final List<String> ingredients,
                  final String sourceUrl, @NonNull final String recipeId, final String imageUrl,
                  final Double socialRank, final String publisherUrl, final String title) {
        this.publisher = publisher;
        this.f2fUrl = f2fUrl;
        //this.ingredients = ingredients;
        this.sourceUrl = sourceUrl;
        this.recipeId = recipeId;
        this.imageUrl = imageUrl;
        this.socialRank = socialRank;
        this.publisherUrl = publisherUrl;
        this.title = title;
    }

    @Ignore
    public static Recipe getDefaultRecipe() {
        final String defaultString = "default";
        final String defaultUrl = "http://" + defaultString;
        return new Recipe(defaultString, defaultUrl ,
                defaultUrl, defaultString,
                defaultUrl, 0.0,
                defaultUrl, defaultString);
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getF2fUrl() {
        return f2fUrl;
    }

    public void setF2fUrl(String f2fUrl) {
        this.f2fUrl = f2fUrl;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getSocialRank() {
        return socialRank;
    }

    public void setSocialRank(Double socialRank) {
        this.socialRank = socialRank;
    }

    public String getPublisherUrl() {
        return publisherUrl;
    }

    public void setPublisherUrl(String publisherUrl) {
        this.publisherUrl = publisherUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "recipeId='" + recipeId + '\'' +
                ", publisher='" + publisher + '\'' +
                ", f2fUrl='" + f2fUrl + '\'' +
                ", sourceUrl='" + sourceUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", socialRank=" + socialRank +
                ", publisherUrl='" + publisherUrl + '\'' +
                ", title='" + title + '\'' +
                ", ingredients=" + ingredients +
                '}';
    }
}
