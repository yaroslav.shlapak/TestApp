package com.schibsted.testapp.presentation;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.schibsted.testapp.domain.usecases.SearchRecipesUseCase;
import com.schibsted.testapp.model.Recipe;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.schibsted.testapp.utils.Constants.FIRST_PAGE;

public class SearchRecipesViewModel extends ViewModel {

    private final SearchRecipesUseCase searchRecipesUseCase;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<List<Recipe>> recepiesMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Integer> pageMutableLiveData = new MutableLiveData<>();

    public SearchRecipesViewModel(SearchRecipesUseCase searchRecipesUseCase) {
        this.searchRecipesUseCase = searchRecipesUseCase;
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public LiveData<List<Recipe>> getRecipesLiveData() {
        return recepiesMutableLiveData;
    }

    public void searchRecipes(final String searchText, final int page) {
        setPage(page);
        disposables.add(searchRecipesUseCase.searchRecepies(searchText, page)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(((recepies) -> {
                    if (page > FIRST_PAGE) {
                        List<Recipe> tempRecipes = recepiesMutableLiveData.getValue();
                        if (tempRecipes == null) {
                            tempRecipes = new ArrayList<>();
                            Timber.w("Shouldn't be reachable, " +
                                    "something wrong on page: %d", page);
                        }
                        tempRecipes.addAll(recepies);
                        recepiesMutableLiveData.postValue(tempRecipes);
                    } else if (page == FIRST_PAGE) {
                        recepiesMutableLiveData.postValue(recepies);
                    } else {
                        Timber.e("incorrect page: %d", page);
                    }
                }
                ), t -> Timber.e(t, "searchRecipes error")));
    }

    public Integer getPage() {
        Integer page = pageMutableLiveData.getValue();
        if (page == null) {
            return FIRST_PAGE;
        } else {
            return page;
        }
    }

    private void setPage(final int page) {
        this.pageMutableLiveData.setValue(page);
    }
}
