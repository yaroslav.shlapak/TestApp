package com.schibsted.testapp.presentation;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.schibsted.testapp.domain.usecases.SearchRecipesUseCase;


public class SearchRecipesViewModelFactory implements ViewModelProvider.Factory {

    private final SearchRecipesUseCase searchRecipesUseCase;

    public SearchRecipesViewModelFactory(SearchRecipesUseCase searchRecipesUseCase) {
        this.searchRecipesUseCase = searchRecipesUseCase;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SearchRecipesViewModel.class)) {
            return (T) new SearchRecipesViewModel(searchRecipesUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
