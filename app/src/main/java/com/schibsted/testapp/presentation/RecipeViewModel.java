package com.schibsted.testapp.presentation;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.schibsted.testapp.domain.usecases.GetRecipeUseCase;
import com.schibsted.testapp.model.Recipe;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class RecipeViewModel extends ViewModel {

    private final GetRecipeUseCase getRecipeUseCase;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<Recipe> recipeLiveData = new MutableLiveData<>();

    public RecipeViewModel(GetRecipeUseCase getRecipeUseCase) {
        this.getRecipeUseCase = getRecipeUseCase;
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public LiveData<Recipe> getRecipeLiveData() {
        return recipeLiveData;
    }

    public void performGetRecipe(final String recipeId) {
        Timber.d("performing with performGetRecipe");
        disposables.add(getRecipeUseCase.getRecipe(recipeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(((user) -> recipeLiveData.setValue(user)),
                        t -> Timber.e(t, "performGetRecipe error")));
    }

    public void performGetRecipeFromDb(final String recipeId) {
        Timber.d("performing with performGetRecipe");
        disposables.add(getRecipeUseCase.getRecipeFromDb(recipeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(((user) -> recipeLiveData.setValue(user)),
                        t -> Timber.e(t, "performGetRecipe error")));
    }

}

