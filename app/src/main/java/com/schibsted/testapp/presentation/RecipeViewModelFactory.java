package com.schibsted.testapp.presentation;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.schibsted.testapp.domain.usecases.GetRecipeUseCase;


public class RecipeViewModelFactory implements ViewModelProvider.Factory {

    private final GetRecipeUseCase getRecipeUseCase;

    public RecipeViewModelFactory(GetRecipeUseCase getRecipeUseCase) {
        this.getRecipeUseCase = getRecipeUseCase;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(RecipeViewModel.class)) {
            return (T) new RecipeViewModel(getRecipeUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
