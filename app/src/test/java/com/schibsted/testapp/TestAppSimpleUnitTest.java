package com.schibsted.testapp;

import com.schibsted.testapp.domain.usecases.GetRecipeUseCase;
import com.schibsted.testapp.domain.usecases.SearchRecipesUseCase;
import com.schibsted.testapp.utils.Constants;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;


public class TestAppSimpleUnitTest {
    @Test
    public void searchTextValidatorIsCorrect() throws Exception {
        for (int i = 0; i < 100; i++) {
            assertEquals(SearchRecipesUseCase.
                    isValid(generateTestSpacesString(null)), false);
        }
        for (int i = 0; i < 100; i++) {
            assertEquals(SearchRecipesUseCase.
                    isValid(generateTestSpacesString("here")), true);
        }
        assertEquals(SearchRecipesUseCase.isValid(""), false);
        assertEquals(SearchRecipesUseCase.isValid(" "), false);
        assertEquals(SearchRecipesUseCase.isValid("     "), false);
        assertEquals(SearchRecipesUseCase.isValid("  1   "), true);
        assertEquals(SearchRecipesUseCase.isValid("  +   "), true);
        assertEquals(SearchRecipesUseCase.isValid(null), false);
    }

    @Test
    public void recipeIdIsCorrect() throws Exception {
        for (int i = 0; i < 100; i++) {
            assertEquals(GetRecipeUseCase.
                    isValid(generateRandomStringWithLenght(
                            Constants.RECIPE_ID_LENGTH)), true);
        }
        for (int i = 0; i < 100; i++) {
            assertEquals(GetRecipeUseCase.
                    isValid(generateRandomStringWithLenght(
                            Constants.RECIPE_ID_LENGTH - 1)), false);
        }
        for (int i = 0; i < 100; i++) {
            assertEquals(GetRecipeUseCase.
                    isValid(generateRandomStringWithLenght(
                            Constants.RECIPE_ID_LENGTH + 1)), true);
        }
        assertEquals(GetRecipeUseCase.isValid("1"), false);
        assertEquals(GetRecipeUseCase.isValid("12"), true);
        assertEquals(GetRecipeUseCase.isValid("123"), true);
        assertEquals(GetRecipeUseCase.isValid(null), false);
    }

    private String generateTestSpacesString(String someSring) {
        StringBuilder stringBuilder= new StringBuilder();
        for (int i = 0; i < 100 * Math.random(); i++) {
            stringBuilder.append(" ");
        }
        if(someSring != null) {
            stringBuilder.insert(stringBuilder.length() / 2, someSring);
        }
        return stringBuilder.toString();
    }

    private String generateRandomStringWithLenght(int length) {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < length; i++) {
            char c = (char)(r.nextInt((int)(Character.MAX_VALUE)));
            sb.append(c);
        }
        return sb.toString();
    }

}